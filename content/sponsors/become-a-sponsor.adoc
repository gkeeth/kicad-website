+++
title = "Become a KiCad Corporate Sponsor"
date = "2021-04-20"
categories = [ "Sponsors" ]
weight = 10
summary = "Become a KiCad Corporate Sponsor"
+++


The KiCad project relies on the support of our members and contributions from the user community to develop and grow the KiCad EDA suite. We encourage companies that benefit from KiCad to support the KiCad community through the KiCad Corporate Sponsor Program. The generous support of these Corporate Sponsors allows the KiCad project to provide a world-class EDA package available to everyone.

A company may become a corporate sponsor by making a financial contribution or an in-kind contribution of goods and services to KiCad. There are four tiers of sponsorship: Platinum, Gold, Silver and Bronze; each tier representing the level of annual sponsorship to KiCad.

[discrete]
=== Platinum Sponsorship

 * Annual contribution of at least US$15,000
 * Corporate logo with supporting quote placed on the KiCad Sponsor page.
 * Recognition during official KiCad presentations (FOSDEM, KiCon, etc)
 * Joint-press release issued between the KiCad Project and Platinum sponsor.
    
[discrete]
=== Gold Sponsorship

 * Annual contribution of at least US$7,500
 * Corporate logo with supporting quote placed on the KiCad Sponsor page.
 * Supporting press quote from the KiCad Project for sponsor announcement.

[discrete]
=== Silver Sponsorship

 * Annual contribution of at least US$3,000
 * Corporate logo with supporting quote placed on the KiCad Sponsor page.

[discrete]
=== Bronze Sponsorship
    
 * Annual contribution of at least US$1,000
 * Corporate logo recognized on the KiCad Sponsor page.

== How to Become a Corporate Sponsor

[discrete]
 . Determine the level of sponsorship your company would like to make.
 . link:++mailto:sponsorship@kicad.org++[Contact the KiCad Project] to determine method of payment.  Please note that KiCad operates under the auspices of The Linux Foundation as a not-for-profit corporation.  Contributions can not be considered charitiable donations.
 . Corporate sponsors, by signing our logo agreement, agree to have their corporate logo listed on the KiCad Sponsor page. If you do not want your logo listed, please inform us.
