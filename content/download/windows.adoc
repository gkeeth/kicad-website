+++
title = "Windows Downloads"
distro = "Windows"
summary = "Downloads KiCad for Windows 8.1, Windows 10 and Windows 11"
iconhtml = "<div><i class='fab fa-windows'></i></div>"
weight = 2
+++

[.initial-text]
KiCad supports  Windows 8.1, 10 and 11.  See
link:/help/system-requirements/[System Requirements] for more details.

[.initial-text]
== Stable Release

Current Version: *6.0.0*

++++
<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="accordion-item">
		<div class="accordion-header" role="tab" id="mirrors-64bit-heading">
			<button role="button" class="accordion-button" data-bs-toggle="collapse" data-parent="#accordion" href="#mirrors-64bit" aria-expanded="true" aria-controls="mirrors-64bit">
					64-bit (recommended)
			</button>
		</div>
		<div id="mirrors-64bit" class="accordion-collapse collapse show" role="tabpanel" aria-labelledby="mirrors-64bit-heading">
			<div class="accordion-body">
				<h4>Worldwide</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link osdn" href="https://osdn.net/projects/kicad/storage/kicad-6.0.0-x86_64.exe">
						<img src="//osdn.net/sflogo.php?group_id=12159&type=1" width="96" height="31"  border="0" alt="OSDN"> OSDN
					</a>
				</div>
				<h4>Europe</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-6.0.0-x86_64.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/windows/stable/kicad-6.0.0-x86_64.exe">
						Futureware - Austria
					</a>
				</div>
				<h4>China</h4>
				<div class="list-group download-list-group">

					<a class="list-group-item dl-link" href="https://mirrors.aliyun.com/kicad/windows/stable/kicad-6.0.0-x86_64.exe">
						<img src="/img/download/aliyun.png" border="0" alt="AliCloud" />AlibabaCloud
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-6.0.0-x86_64.exe">
						<img src="/img/download/chongqing.jpeg" />Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.dgut.edu.cn/kicad/windows/stable/kicad-6.0.0-x86_64.exe">
						<img src="/img/download/dgut.png" />Dongguan University of Technology
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-6.0.0-x86_64.exe">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="accordion-item">
		<div class="accordion-header" role="tab" id="mirrors-32bit-heading">
			<button role="button" class="accordion-button" data-bs-toggle="collapse" data-parent="#accordion" href="#mirrors-32bit" aria-expanded="false" aria-controls="mirrors-32bit">
					32-bit
			</button>
		</div>
		<div id="mirrors-32bit" class="accordion-collapse collapse" role="tabpanel" aria-labelledby="mirrors-32bit-heading">
			<div class="accordion-body">
				<h4>Worldwide</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link osdn" href="https://osdn.net/projects/kicad/storage/kicad-6.0.0-i686.exe">
						<img src="//osdn.net/sflogo.php?group_id=12159&type=1" width="96" height="31"  border="0" alt="OSDN"> OSDN
					</a>
				</div>
				<h4>Europe</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-6.0.0-i686.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/windows/stable/kicad-6.0.0-i686.exe">
						Futureware - Austria
					</a>
				</div>
				<h4>China</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://mirrors.aliyun.com/kicad/windows/stable/kicad-6.0.0-i686.exe">
						<img src="/img/download/aliyun.png" border="0" alt="AliCloud" /> AlibabaCloud
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-6.0.0-i686.exe">
						<img src="/img/download/chongqing.jpeg" /> Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.dgut.edu.cn/kicad/windows/stable/kicad-6.0.0-i686.exe">
						<img src="/img/download/dgut.png" />Dongguan University of Technology
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-6.0.0-i686.exe">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
++++

[.donate-hidden]
== {nbsp}
++++
	{{< getpartial "download_thanks.html" >}}
++++



== Download Verification
All installer binaries will have a code signed digital signature attached. Windows will automatically verify the signature is valid, however you may want
to ensure it actually is present and the correct signer. A guide on how to verify the installer is available here: link:/help/windows-download-verification/[Windows Installer Verification Guide]

A valid official KiCad signature has this certificate info:

[role="table table-striped table-condensed"]
|===
|Signer Name|*KiCad Services Corporation*
|Issuer|*Sectigo RSA Code Signing CA*
|Serial Number|*1f70b098b5c21a254a6fb427cdf8893e*
|===

However, older builds maintain this valid certificate info:

[role="table table-striped table-condensed"]
|===
|Signer Name|*Simon Richter*
|Issuer|*DigiCert SHA2 Assured ID Code Signing CA*
|Serial Number|*08d72a8ce400d3b9384a263511bac45d*
|===


== Previous Releases

Previous releases should be available for download on:

https://downloads.kicad.org/kicad/windows/explore/stable


== Testing Builds

The _testing_ builds are snapshots of the current stable release codebase at a specific time.
These contain the most recent bugfixes that will be included in the next stable release.

https://downloads.kicad.org/kicad/windows/explore/5.1-testing


== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a specific time.
This codebase is under active development, and while we try our best, may contain more bugs than usual.
New features added to KiCad can be tested in these builds.

WARNING: These builds may be unstable, and projects edited with these are not usable with the current stable release. **Use at your own risk**

https://downloads.kicad.org/kicad/windows/explore/nightlies
